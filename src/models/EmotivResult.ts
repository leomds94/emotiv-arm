export class EmotivResult {
  id: number;
  result: any[];


  public constructor(init?: Partial<EmotivResult>) {
    this.id = null;
    this.result = [];

    if (init) {
      Object.assign(this, init);
      }
  }

}
