export class EmotivUser {
  username: string;
  password: string;
  client_id: string;
  client_secret: string;

  public constructor(init?: Partial<EmotivUser>) {
    this.username = '';
    this.password = '';
    this.client_id = '';
    this.client_secret = '';

    if (init) {
      Object.assign(this, init);
      }
  }

}
