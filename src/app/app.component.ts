import { EmotivResult } from "./../models/EmotivResult";
import { Component } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { EmotivUser } from "src/models/EmotivUser";
import { Stopwatch } from "ts-stopwatch";
// import { timer } from 'rxjs/observable/timer';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  users = [];
  selectedUser = new EmotivUser();
  time = 5;
  run_mlp = false;
  train_mlp = false;
  is_trained = false;
  port = 5002;
  stopwatch = new Stopwatch();
  headsetConnected = false;
  // source = timer(0, 5000);

  running_gesture = "";

  training = [];

  constructor(private http: HttpClient) {
    // tslint:disable-next-line:max-line-length
    this.selectedUser = {
      username: "leoccm94",
      password: "Leo556005",
      client_id: "MbGdsB2deBrFlsy8r7ORpgdxpUePV9Fh4FvIivf0",
      client_secret:
        "tnr8kvsBVGNFhhirdTDcsfOWN577XmRxvpddcXHikeiW7UFmtanjmS1VrrLfNsMd02l2lBvcuJVwo4MZwQLMpzfRt6dsLnH0n9GziUbsl54CnoHQ1aZcPxRgQNZgJOrw"
    };

    this.GetLoggedUser();
    this.VerifyMlpTrained();

    this.training["neutral"] = 0;
    this.training["grab"] = 0;
    this.training["rock"] = 0;
    this.training["cool"] = 0;
    this.training["thumb"] = 0;
    this.training["forearm"] = 0;
  }

  VerifyMlpTrained() {
    this.http
      .get("http://127.0.0.1:" + this.port + "/istrained")
      .subscribe((data: string) => {
        this.is_trained = Boolean(data);
      });
  }

  RunMlp() {
    this.run_mlp = true;
    this.http
      .get("http://127.0.0.1:" + this.port + "/runmlp")
      .subscribe((data: string) => {
        var result = JSON.parse(data) as EmotivResult;
        console.log(result);
      });
  }

  StopMlp() {
    this.http
      .get("http://127.0.0.1:" + this.port + "/stopmlp")
      .subscribe((data: string) => {
        var result = JSON.parse(data) as EmotivResult;
        console.log(result);
        this.run_mlp = false;
      });
  }

  TrainMlp() {
    this.train_mlp = true;
    this.http
      .get("http://127.0.0.1:" + this.port + "/trainmlp")
      .subscribe((data: string) => {
        var result = JSON.parse(data) as EmotivResult;
        console.log(result);
        this.train_mlp = false;
        this.VerifyMlpTrained();
      });
  }

  GetLoggedUser() {
    this.http
      .get("http://127.0.0.1:" + this.port + "/getuserlogin")
      .subscribe((data: string) => {
        var result = JSON.parse(data) as EmotivResult;
        this.users = result.result;
      });
  }

  EmotivLogin() {
    this.http
      .get(
        "http://127.0.0.1:" +
          this.port +
          "/login/" +
          JSON.stringify(this.selectedUser)
      )
      .subscribe(data => {
        this.headsetConnected =
          data.toString().indexOf("No headset connected") >= 0 ? false : true;
        console.log(data);
        this.GetLoggedUser();
      });
  }

  EmotivLogout() {
    var result: EmotivResult;
    this.http
      .get("http://127.0.0.1:" + this.port + "/getuserlogin")
      .subscribe((data: string) => {
        result = JSON.parse(data) as EmotivResult;

        for (let res of result.result) {
          this.http
            .get("http://127.0.0.1:" + this.port + "/logout/" + res)
            .subscribe(data => {
              console.log(data as JSON);
            });
        }
        this.users = [];
      });
  }

  EmotivLogoutAll() {
    this.http
      .get("http://127.0.0.1:" + this.port + "/logoutall")
      .subscribe(data => {
        console.log(data as JSON);
      });
    this.users = [];
  }

  EmotivTraining(gesture, command) {
    this.running_gesture = command;
    // const subscribe = this.source.subscribe(val => console.log(val));
    this.http
      .get(
        "http://127.0.0.1:" + this.port + "/populatecsv/" + this.time + "/" + gesture + "/" + command
      )
      .subscribe(data => {
        console.log(data as JSON);
        this.running_gesture = "";
        // subscribe.unsubscribe();
      });

    this.training[command]++;
  }

  SelectUser(user) {
    this.selectedUser = user;
  }
}
